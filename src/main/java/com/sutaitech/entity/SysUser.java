package com.sutaitech.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户，与compnay用户共用，包括系统的用户，个人客户，企业客户对象
 * @email: lyg945@163.com
 * @author: Yonggang.Liu
 * @date: 2015-09-22 10:20:55
 */
public class SysUser implements Serializable {
       
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	
	private String loginName;
	
	private String loginPwd;
	
	private Integer companyId;
	
	private String showName;
	
	private Integer orgId;
	
	private String orgName;
	
	//邮箱，必须唯一，用作密码找回
	private String email;
	
	//DICT_GLOBAL_YESNO 是Y，否N
	private String verEmailFlag;
	
	private Date verEmailDate;
	
	private String firstLang;
	
	private String selTheme;
	
	private Date firstVisitDt;
	
	private Date preVisitDt;
	
	private Date lastVisitDt;
	
	private Date lastErrDt;
	
	private String firstVisitIp;
	
	private String preVisitIp;
	
	private String lastVisitIp;
	
	private String lastErrIp;
	
	private Integer loginCount;
	
	//DICT_GLOBAL_YESNO 是Y，否N
	private String employeeFlag;
	
	//DICT_SYS_ONLINE_STATUS：ONL在线、OFL离线、HID隐身
	private String onlineStatus;
	
	private String telephone;
	
	private String mp;
	
	//DICT_GLOBAL_YESNO 是Y，否N
	private String mpValFlag;
	
	private Date mpValTime;
	
	//DICT_SYS_USER_TYPE：COM企业客户、AGT代理商、PEF个人免费客户、PEM个人会员客户、PEV个人VIP客户
	private String userType;
	
	//DICT_GLOBAL_YESNO 是Y，否N
	private String questionFlag;
	
	private String miscDesc;
	
	//状态：DICT_GLOBAL_STATUS 有效V、无效I、草稿D、待审核W
	private String status;
	
	private Date createTime;
	
	private Integer createOperId;
	
	private String createOperName;
	
	private Date lastModTime;
	
	private Integer lastModOperId;
	
	private String lastModOperName;
	
	private String createdDateStr;
	private String updatedDateStr;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getLoginPwd() {
		return loginPwd;
	}

	public void setLoginPwd(String loginPwd) {
		this.loginPwd = loginPwd;
	}
	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public String getShowName() {
		return showName;
	}

	public void setShowName(String showName) {
		this.showName = showName;
	}
	public Integer getOrgId() {
		return orgId;
	}

	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}
	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	public String getVerEmailFlag() {
		return verEmailFlag;
	}

	public void setVerEmailFlag(String verEmailFlag) {
		this.verEmailFlag = verEmailFlag;
	}
	public Date getVerEmailDate() {
		return verEmailDate;
	}

	public void setVerEmailDate(Date verEmailDate) {
		this.verEmailDate = verEmailDate;
	}
	public String getFirstLang() {
		return firstLang;
	}

	public void setFirstLang(String firstLang) {
		this.firstLang = firstLang;
	}
	public String getSelTheme() {
		return selTheme;
	}

	public void setSelTheme(String selTheme) {
		this.selTheme = selTheme;
	}
	public Date getFirstVisitDt() {
		return firstVisitDt;
	}

	public void setFirstVisitDt(Date firstVisitDt) {
		this.firstVisitDt = firstVisitDt;
	}
	public Date getPreVisitDt() {
		return preVisitDt;
	}

	public void setPreVisitDt(Date preVisitDt) {
		this.preVisitDt = preVisitDt;
	}
	public Date getLastVisitDt() {
		return lastVisitDt;
	}

	public void setLastVisitDt(Date lastVisitDt) {
		this.lastVisitDt = lastVisitDt;
	}
	public Date getLastErrDt() {
		return lastErrDt;
	}

	public void setLastErrDt(Date lastErrDt) {
		this.lastErrDt = lastErrDt;
	}
	public String getFirstVisitIp() {
		return firstVisitIp;
	}

	public void setFirstVisitIp(String firstVisitIp) {
		this.firstVisitIp = firstVisitIp;
	}
	public String getPreVisitIp() {
		return preVisitIp;
	}

	public void setPreVisitIp(String preVisitIp) {
		this.preVisitIp = preVisitIp;
	}
	public String getLastVisitIp() {
		return lastVisitIp;
	}

	public void setLastVisitIp(String lastVisitIp) {
		this.lastVisitIp = lastVisitIp;
	}
	public String getLastErrIp() {
		return lastErrIp;
	}

	public void setLastErrIp(String lastErrIp) {
		this.lastErrIp = lastErrIp;
	}
	public Integer getLoginCount() {
		return loginCount;
	}

	public void setLoginCount(Integer loginCount) {
		this.loginCount = loginCount;
	}
	public String getEmployeeFlag() {
		return employeeFlag;
	}

	public void setEmployeeFlag(String employeeFlag) {
		this.employeeFlag = employeeFlag;
	}
	public String getOnlineStatus() {
		return onlineStatus;
	}

	public void setOnlineStatus(String onlineStatus) {
		this.onlineStatus = onlineStatus;
	}
	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getMp() {
		return mp;
	}

	public void setMp(String mp) {
		this.mp = mp;
	}
	public String getMpValFlag() {
		return mpValFlag;
	}

	public void setMpValFlag(String mpValFlag) {
		this.mpValFlag = mpValFlag;
	}
	public Date getMpValTime() {
		return mpValTime;
	}

	public void setMpValTime(Date mpValTime) {
		this.mpValTime = mpValTime;
	}
	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getQuestionFlag() {
		return questionFlag;
	}

	public void setQuestionFlag(String questionFlag) {
		this.questionFlag = questionFlag;
	}
	public String getMiscDesc() {
		return miscDesc;
	}

	public void setMiscDesc(String miscDesc) {
		this.miscDesc = miscDesc;
	}
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Integer getCreateOperId() {
		return createOperId;
	}

	public void setCreateOperId(Integer createOperId) {
		this.createOperId = createOperId;
	}
	public String getCreateOperName() {
		return createOperName;
	}

	public void setCreateOperName(String createOperName) {
		this.createOperName = createOperName;
	}
	public Date getLastModTime() {
		return lastModTime;
	}

	public void setLastModTime(Date lastModTime) {
		this.lastModTime = lastModTime;
	}
	public Integer getLastModOperId() {
		return lastModOperId;
	}

	public void setLastModOperId(Integer lastModOperId) {
		this.lastModOperId = lastModOperId;
	}
	public String getLastModOperName() {
		return lastModOperName;
	}

	public void setLastModOperName(String lastModOperName) {
		this.lastModOperName = lastModOperName;
	}
	public String getCreatedDateStr() {
		return createdDateStr;
	}
	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	public String getUpdatedDateStr() {
		return updatedDateStr;
	}
	public void setUpdatedDateStr(String updatedDateStr) {
		this.updatedDateStr = updatedDateStr;
	}
	
	@Override
	public String toString() {
		return String
				.format("SysUser [id=%s, loginName=%s, loginPwd=%s, companyId=%s, showName=%s, orgId=%s, orgName=%s, email=%s, verEmailFlag=%s, verEmailDate=%s, firstLang=%s, selTheme=%s, firstVisitDt=%s, preVisitDt=%s, lastVisitDt=%s, lastErrDt=%s, firstVisitIp=%s, preVisitIp=%s, lastVisitIp=%s, lastErrIp=%s, loginCount=%s, employeeFlag=%s, onlineStatus=%s, telephone=%s, mp=%s, mpValFlag=%s, mpValTime=%s, userType=%s, questionFlag=%s, miscDesc=%s, status=%s, createTime=%s, createOperId=%s, createOperName=%s, lastModTime=%s, lastModOperId=%s, lastModOperName=%s, createdDateStr=%s, updatedDateStr=%s]",
						id, loginName, loginPwd, companyId, showName, orgId,
						orgName, email, verEmailFlag, verEmailDate, firstLang,
						selTheme, firstVisitDt, preVisitDt, lastVisitDt,
						lastErrDt, firstVisitIp, preVisitIp, lastVisitIp,
						lastErrIp, loginCount, employeeFlag, onlineStatus,
						telephone, mp, mpValFlag, mpValTime, userType,
						questionFlag, miscDesc, status, createTime,
						createOperId, createOperName, lastModTime,
						lastModOperId, lastModOperName, createdDateStr,
						updatedDateStr);
	}
}


